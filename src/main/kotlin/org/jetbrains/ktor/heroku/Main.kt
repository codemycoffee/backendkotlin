package org.jetbrains.ktor.heroku

import com.google.gson.Gson
import com.zaxxer.hikari.*
import freemarker.cache.*
import org.jetbrains.ktor.application.*
import org.jetbrains.ktor.content.*
import org.jetbrains.ktor.features.*
import org.jetbrains.ktor.freemarker.*
import org.jetbrains.ktor.host.*
import org.jetbrains.ktor.http.*
import org.jetbrains.ktor.netty.*
import org.jetbrains.ktor.routing.*
import org.jetbrains.ktor.response.respondText

fun Application.module() {


    install(Routing) {
        serveClasspathResources("public")


        get("/") {
            val adminData = generateDB()
            val gson = Gson()
            val json = gson.toJson(adminData)
            call.respondText(json, ContentType.Application.Json)
        }
    }
}

fun main(args: Array<String>) {
    val port = Integer.valueOf(System.getenv("PORT"))
    embeddedServer(Netty, port, reloadPackages = listOf("heroku"), module = Application::module).start()
}


