package org.jetbrains.ktor.heroku

val steps0: ArrayList<Step> = arrayListOf(
        Step("something to do", true),
        Step("things that will be done", true),
        Step("another mission to accomplish and stuff", false),
        Step("just a couple more to test", false),
        Step("this is the last thing to do, worry not", false)
)

val steps1: ArrayList<Step> = arrayListOf(
        Step("something to do", true),
        Step("things that will be done", true),
        Step("another mission to accomplish and stuff", true),
        Step("just a couple more to test", false),
        Step("this is the last thing to do, worry not", false)
)

val steps2: ArrayList<Step> = arrayListOf(
        Step("something to do", true),
        Step("things that will be done", false),
        Step("another mission to accomplish and stuff", false),
        Step("just a couple more to test", false),
        Step("this is the last thing to do, worry not", false)
)

val steps3: ArrayList<Step> = arrayListOf(
        Step("something to do", false),
        Step("things that will be done", false),
        Step("another mission to accomplish and stuff", false),
        Step("just a couple more to test", false),
        Step("this is the last thing to do, worry not", false)
)

val steps4: ArrayList<Step> = arrayListOf(
        Step("something to do", true),
        Step("things that will be done", true),
        Step("another mission to accomplish and stuff", true),
        Step("just a couple more to test", true),
        Step("this is the last thing to do, worry not", false)
)

val steps: Array<ArrayList<Step>> = arrayOf(steps0, steps1, steps2, steps3, steps4)
val phones: Array<String> = arrayOf(
        "+33625456589",
        "+33700212032",
        "+33775369586",
        "+33677879868",
        "+33633204523"
)
val notes: Array<String> = arrayOf(
        "Need to confirm with the candidate next step",
        "Call to proceed to next step",
        "Waiting for email answer",
        "Awaiting for VISA confirmation",
        "Just sent the contract to be checked"
)

val names: Array<String> = arrayOf("John Mayer", "Mr Incredible", "That dude", "Jeanne D'arc", "Happiness Yarlhe")

fun generateDB(): Admin {
    val admin = Admin("Sir Admin")

    var users: ArrayList<User> = ArrayList()
    names.forEachIndexed { index, name -> users.add(generateUser(name, index)) }
    admin.users = users

    return admin
}

fun generateUser(userName: String, index: Int): User {
    val process = Process("Tunisia recruitment", steps[index])
    return User(userName, "$userName@email.com",
            phones[index], process, notes[index])
}