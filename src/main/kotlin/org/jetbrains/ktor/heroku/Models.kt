package org.jetbrains.ktor.heroku

data class Step(val description: String, var isDone: Boolean)

data class Process(val name: String, var steps: ArrayList<Step>)

data class User(val name: String, val mail: String, val number: String, val process: Process, var notes:String)

data class Admin(val name: String = "", var users: ArrayList<User> = ArrayList())